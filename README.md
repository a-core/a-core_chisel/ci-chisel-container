# ci-chisel-container

Docker container with JDK and sbt installed for executing Chisel unit testers.
The container image also contains git for fetching submodules and make for
executing test defined as make recipes.

This container is supposed to be small enough to be able to run on Gitlab small runners.

