FROM ubuntu:22.04
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y \
    git \
    make \
    wget \
    gnupg \
    curl \
    unzip \
    build-essential \
    python3 \
    python3-pip \
    default-jdk

# Install sbt
RUN apt-get update && \
    apt-get install apt-transport-https curl gnupg -yqq && \
    echo "deb https://repo.scala-sbt.org/scalasbt/debian all main" | tee /etc/apt/sources.list.d/sbt.list && \
    echo "deb https://repo.scala-sbt.org/scalasbt/debian /" | tee /etc/apt/sources.list.d/sbt_old.list && \
    curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x2EE0EA64E40A89B84B2DF73499E82A75642AC823" | gpg --no-default-keyring --keyring    gnupg-ring:/etc/apt/trusted.gpg.d/scalasbt-release.gpg --import && \
    chmod 644 /etc/apt/trusted.gpg.d/scalasbt-release.gpg && \
    apt-get update && \
    apt-get install sbt

# Install oss-cad-suite
RUN wget -q https://github.com/YosysHQ/oss-cad-suite-build/releases/download/2023-10-30/oss-cad-suite-linux-x64-20231030.tgz
RUN tar -xf oss-cad-suite-linux-x64-20231030.tgz
ENV PATH=/oss-cad-suite/bin:/oss-cad-suite/py3bin:$PATH

# Install TestFloat and SoftFloat
RUN mkdir TestFloat
RUN cd /TestFloat && \
wget "http://www.jhauser.us/arithmetic/SoftFloat-3e.zip" && \
wget "http://www.jhauser.us/arithmetic/TestFloat-3e.zip" && \
unzip -q SoftFloat-3e.zip && \
unzip -q TestFloat-3e.zip
RUN cd /TestFloat/SoftFloat-3e/build/Linux-x86_64-GCC && make
RUN cd /TestFloat/TestFloat-3e/build/Linux-x86_64-GCC && make
RUN rm -rf TestFloat/SoftFloat-3e.zip
RUN rm -rf TestFloat/TestFloat-3e.zip
ENV TESTFLOAT_PATH=/TestFloat/TestFloat-3e/build/Linux-x86_64-GCC/

RUN python3 -m pip install \
    wheel \
    gnureadline \
    numpy \
    numpydoc \
    matplotlib \
    joblib \
    scipy \
    pandas \
    sphinx \
    sphinx_rtd_theme \
    pyelftools \
    sortedcontainers \
    bitstring \
    pyyaml \
    cocotb \
    cocotb_test \
    pytest \
    cocotbext-axi \
    cocotbext-uart \
    sphinx


